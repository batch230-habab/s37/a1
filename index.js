const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");

// to create an express server/application
const app = express();

// middlewares - allows to bridge our backend application(server) to our front end
app.use(cors()); // to allow cross origin resource sharing
app.use(express.json()); // to read json objects
app.use(express.urlencoded({extended:true})); // to read forms
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

// mongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch230.dzsjady.mongodb.net/courseBooking?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
})
mongoose.connection.once("open", () => console.log("Now connected to Habab-Mongo DB Atlas"))

app.listen(process.env.PORT || 4000, () => 
    {
        console.log (`API is now online on port ${process.env.PORT || 4000}`)
    });