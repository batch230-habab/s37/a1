const mongoose = require("mongoose");
const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

// ? Function for adding a course
// ! 2. update the "addCourse" controller method to implement admin authentication for creating a course
// ! NOTE: include screenshot of successful admin addCourse and screenshot of not successful add course by a user that is not admin
/* module.exports.addCourse = (reqBody) => {
    let newCourse = new Course({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })

    return newCourse.save()
    .then((newCourse, error) => {
        if(error){
            return error;
        }else{
            return newCourse;
        }
    })
} */
/* module.exports.addCourse = (reqBody, newData) => {
    if(newData.isAdmin == true){
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        })
        return newCourse.save().then((newCourse,error)=>{
            if(error){
                return error;
            }else{
                return newCourse;
            }
        })
    }else{
        let message = Promise.resolve('User must be ADMIN to access this functionality');
        return message.then((value) => {return value});
    }
} */

module.exports.addCourse = (req, res) => {
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
	if(adminCheck == true){
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price
		})
		newCourse.save()
		.then(result => res.send(result))
		.catch(error => res.send(error));
	}else{
		let message = 'User does not have Admin privileges';
		return res.send(message);
	}
}

// ? GET all course
module.exports.getAllCourse = () => {
    return Course.find({}).then(result =>{
        return result;
    })
}

// ? Get all active courses
module.exports.getActiveCourses = () => {
    return Course.find({isActive:true}).then(result =>{
        return result;
    })
}

//? GET specific course
module.exports.getCourse = (courseId) => {
    return Course.findById(courseId).then(result =>{
        return result;
    })
}

// *[Additional Example]
// *[Arrow function to regular function]
// *Get ALL course feature converted to regular function
// *'getAllCourseInRegularFunction' acts as a variable/storage of getAllCoursesV2() function so it could be exported
module.exports.getAllCourseInRegularFunction = 
function getAllCoursesV2(){
	return Course.find({}).
	then(
			function sendResult(result)
			{
				return result;
			}
	);
}


//? UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}

//! s40 activity

module.exports.archiveCourse = (req, res) =>{
	const adminCheck = auth.decode(req.headers.authorization).isAdmin
	if(adminCheck == true){
		Course.findByIdAndUpdate(req.params.courseId, {isActive: false}, {new:true})
        //.then(result => res.send(result))
        .then(res.send(true))
		.catch(error => res.send(error));
	}else{
        let message = "User does not have Admin privileges";
        return res.send(message);
    }
}