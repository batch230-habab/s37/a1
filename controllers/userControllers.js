const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10), //10 - salt
        isAdmin: reqBody.isAdmin
    })
    return newUser.save().then((user, error) =>{
            if(error){
                return false;
            }else{
                return newUser;
            }
        }
    )
}

module.exports.checkEmailExist = (reqBody) => {
    return User.find({email: reqBody.email}).then(result => {
        if(result.length >0){
            return true;
        }else{
            return false;
        }
    })
}

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result =>{
        if(result == null){
            return false;
        }else{
            // * compareSync is a bcrypt function to compare unhashed password to hashed password
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); // returns true or false
            // *
            if(isPasswordCorrect){
                // ? give the user a token to access features
                return {access: auth.createAccessToken(result)};
            }else{
                return false;
            }
        }
    })
}

module.exports.getProfile = (req, res) =>{
    User.findById(req.body.id)
    .then(result => {
        result.password = "ffffffffffff";
        res.send(result);
    })
    .catch(error => res.send(error));
}