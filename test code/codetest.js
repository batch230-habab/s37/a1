const router = require("../routes/courseRoutes");

module.exports.registerUserController = (req, res) => {
	console.log(req.body);
	User.findOne({name: req.body.email}).then(result =>
	{
		console.log(result);
		if(result === null && result.email != req.body.email){
			let newUser = new User({
				email: req.body.email
			})
			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}else{
			return res.send("Email is already in use");
		}
	})
	.catch(error => res.send(error))
};
//!------------------------------------------------------------------------------------




router.post("/details", userControllers.getProfile);

// ? this is for route. BUT it meh

router.post("/create",(req, res) =>{
	courseControllers.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})

// ? this is my first attempt

router.post("/create", auth.verify , (request, response)=>{
	const newData = {
		course:request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	};
courseControllers.addCourse()})

// ? this is for controller
module.exports.addCourse = (req, res) => {
	console.log(req.body);
	let newCourse = new Course({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	});
	if(adminAuth.isAdmin == true){
		newCourse.save()
		.then(result => res.send(result))
		.catch(error =>res.send(error));
	}else{
		let message = Promise.resolve('User has no Administrator privileges');
		return message.then((value) => {return value})
	}
}



module.exports.addCourse = (reqBody, yolo) => {
	console.log(reqBody + 'test');
if(yolo.isAdmin == true){
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	// if(yolo.isAdmin == true){
		newCourse.save()
        .then(result => res.send(result))
		.catch(error =>res.send(error));
	}else{
		let message = Promise.resolve('User has no Administrator privileges');
		return message.then((value) => {return value})
	}
}

module.exports.addCourse = (reqBody, newData) =>{
	if(newData.isAdmin !== true){
		let message = Promise.resolve('User is not an Admin');
		return  message.then((value) => {return value});
	}else{
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newCourse.save()
		.then(result)
	}
}











//! TRY AGAIN BOYYY


//! actual s39 activity solution
//* route

router.post("/create",auth.verify, courseControllers.addCourse)



// * controller

module.exports.addCourse = (req, res) => {
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
	if(adminCheck == true){
		let newCourse = new Course({
			name: req.body.name,
			description: req.body.description,
			price: req.body.price
		})
		newCourse.save()
		.then(result => res.send(result))
		.catch(error => res.send(error));
	}else{
		let message = 'User does not have Admin privileges';
		return res.send(message);
	}
}


module.exports.updateCourse = (req, res) => {
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
	if(adminCheck == true){
		Course.findByIdAndUpdate(courseId,
			{
				name: req.body.name, 
				description: req.body.description,
				price: req.body.price
			}
		)
		.then(result => res.send(result))
		.catch(error => res.send(error));
	}else{
		let message = 'User does not have Admin privileges';
		return res.send(message);
	}
}

// ! activity s40

// * route

router.patch("/:courseId/archive", auth.verify, courseControllers.archiveCourse)

//* controllers

module.exports.archiveCourse = (req, res) =>{
	const adminCheck = auth.decode(req.headers.authorization).isAdmin
	if(adminCheck == true){
		Course.findByIdAndUpdate(req.params.courseId, {isActive: false}, {new:true})
		.then(result => res.send(result))
		.catch(error => res.send(error));
	}
}